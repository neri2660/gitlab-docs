# First use the bootstrap image to build master
FROM registry.gitlab.com/gitlab-com/gitlab-docs:bootstrap as builder

# Set up needed environment variables that are called with --build-arg when
# the Docker image is built (see .gitlab-ci.yml).
ARG NANOC_ENV
ARG CI_COMMIT_REF_NAME
# If CI_COMMIT_REF_NAME is not set (local development), set it to master
ENV CI_COMMIT_REF_NAME ${CI_COMMIT_REF_NAME:-master}

# Build the docs from this branch
COPY . /source/
RUN bundle exec rake setup_git default
RUN bundle exec nanoc compile -VV

# Remove tmp dir to save some space
RUN rm -rf /source/tmp

# BUILD OF MASTER DOCS IS NOW DONE!

# Reset to alpine so we don't get any docs source or extra apps
FROM nginx:1.12-alpine

ENV TARGET=/usr/share/nginx/html

# Get the nginx config from the nginx-onbuild image
# This hardly ever changes so should usually be cached
COPY --from=registry.gitlab.com/gitlab-com/gitlab-docs:nginx-onbuild /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

RUN rm -rf /usr/share/nginx/html/*

# Get all the archive static HTML and put it into place
# Copy the versions found in 'content/_data/versions.yaml' under online.
COPY --from=registry.gitlab.com/gitlab-com/gitlab-docs:12.0 ${TARGET} ${TARGET}
COPY --from=registry.gitlab.com/gitlab-com/gitlab-docs:11.11 ${TARGET} ${TARGET}
COPY --from=registry.gitlab.com/gitlab-com/gitlab-docs:11.10 ${TARGET} ${TARGET}

# Get the built docs output from the previous build stage
# This ordering means all previous layers can come from cache unless an archive
# changes
COPY --from=builder /source/public ${TARGET}

# Serve the site (target), which is now all static HTML
CMD echo -e "GitLab docs are viewable at:\nhttp://0.0.0.0:4000"; exec nginx -g 'daemon off;'
